<?php
namespace ApiNovumOverheid;

use ApiNovumOverheid\Directory\MindMapController;
use Core\IControllerFactory;
use Core\MainController;

class ControllerFactory extends \_DefaultApi\ControllerFactory implements IControllerFactory{

    public function getController(array $aGet, array $aPost, string $sNamespace = null):MainController
    {
        $sRequestUri = $_SERVER['REQUEST_URI'];

        preg_match('/^\/v([0-9]+)/', $sRequestUri, $aMatches);

        $sRequestUri = preg_replace('/^\/(v[0-9]+)/', '', $sRequestUri);


        $oController = null;

        if($sRequestUri == '/directory/mindmap')
        {
            $oController = new MindMapController($aGet, $aPost);
        }

        if(!$oController instanceof MainController)
        {
            return parent::getController($aGet, $aPost, $sNamespace);
        }
        return $oController;
    }

}
