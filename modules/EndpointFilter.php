<?php
namespace ApiNovumOverheid;

use Crud\DataSource\CrudDataSourceManager;
use Crud\FormManager;
use Crud\IApiAlwaysExpose;
use Crud\IApiEndpointFilter;

class EndpointFilter implements IApiEndpointFilter
{

    function filter(FormManager $oManager):bool
    {
        if($oManager instanceof IApiAlwaysExpose)
        {
            return true;
        }

        if($oManager instanceof CrudDataSourceManager)
        {
            return true;
        }
        if(!method_exists($oManager, 'getTags'))
        {
            return false;
        }
        $aTags = $oManager->getTags();

        if(!in_array('NovumOverheid', $aTags))
        {
            return false;
        }

        return true;
    }
}
