<?php
namespace ApiNovumOverheid\Directory;

use Core\MainController;
use Core\Reflector;
use LowCode\App;
use LowCode\Properties;
use LowCode\State;
use LowCode\State\HttpState;

/**
 * Skeleton subclass for drawing a list of DataSource records.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class MindMapController extends MainController
{
    /**
     * @return array|void
     * @throws \Exception
     */
    function run()
    {
        $sDir = (new Reflector($this))->getClassDir();

        $sDefinition = $sDir . '/app.json';
        $sDefinitionJson = file_get_contents($sDefinition);
        $aDefinition = json_decode($sDefinitionJson, true);

        $oApp = new App($aDefinition, array_merge_recursive($_GET, $_POST));
        echo $oApp->render()['result'];
        exit();
    }


}
