<?php

return [
    'api_version' => 2,
    'config_dir'  => 'novum.overheid',
    'namespace'   => 'ApiNovumOverheid',
    'protocol'    => isset($_SERVER['IS_DEVEL']) ? 'http' : 'https',
    'live_domain' => 'api.overheid.demo.novum.nu',
    'dev_domain'  => 'api.overheid.demo.novum.nuidev.nl',
    'test_domain' => 'api.overheid.innovatieapp.nl',
];
